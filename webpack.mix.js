let mix = require('laravel-mix');

mix.setPublicPath('www')
   .js('app/app.js', 'js/app.js')
   .stylus('assets/stylus/app.styl', 'css/app.css');